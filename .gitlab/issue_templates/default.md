Thank you for contributing to the GNOME Project! 
Events are very important for the GNOME community to continue growing and advancing our mission of creating free software that everyone can use. 
We're glad you would like to attend one!

To find information on who can apply for travel sponsorship and what costs are covered, please visit: 
https://wiki.gnome.org/Travel/

Please complete the form below, making sure to add a `x` to all boxes that apply, and completing other fields to the best of your ability, to ensure that your application is processed smoothly. Please pay special attention to the statement of interest, and to confirm the statements at the bottom of the form.


# APPLICATION FORM 
## Application information
* Your name: 
* Employment or project affiliation: 
* Foundation membership status (member / non-member): 

## Event information
* Event name: 
* Role at the event: [ ] Participant  [ ] Speaker  [ ] Organizer  [ ] Other, please specify: 
* Date of arrival: 
* Date of departure: 
* Travelling from: 
* Returning to: 

## Statement of interest (up to 150 words):


## Sponsorship request (attach PDFs/proofs of your searches)
* Estimated travel costs (Attach a screenshot or PDF of your transportation search): 
* Estimated lodging costs (If applicable, attach a screenshot or PDF of your lodging search): 
* Visa application costs: 
* Total costs (total amount you will spend on sponsorable activities): 
* Own contribution (this is how much you are able to put towards the costs, if any): 
* Requested sponsorship total (total costs minus own contribution): 
* Any notes about your requested amount that you would like us to consider (optional): 

## Additional financial assistance
Regular travel sponsorship does not cover meals and other expenses, but we have a limited number of financial assistance packages that cover meals and incidental expenses. Applicants can request one if it would be a financial hardship for them to attend the event without it.

[ ] Check this box if you need a financial assistance package. (If you check this box, put 0 for "own contribution".)

* Please include any information that would help us make a good decision about awarding you a financial assistance package:

## Please check the boxes to indicate your understanding of the conditions
[ ] The GNOME Foundation will not reimburse, compensate or indemnify any risk, expense or liability not explicitly identified and agreed to in the sponsorship agreement. Any additional costs you incur or damages you suffer, even unforseen ones, will not be borne by the GNOME Foundation. You are not covered by any insurance policy held by the GNOME Foundation. You are not an employee of the GNOME Foundation.

[ ] The GNOME Foundation reserves the right to use information about the sponsorship publicly and use it in any advertisements or promotional reasons seen fit by the GNOME Foundation.

[ ] You have read and agree to the conditions of the GNOME Project Travel Sponsorship Policy at https://wiki.gnome.org/Travel/, including the requirement to write a report about the event.


/confidential
